from serial.tools.list_ports import comports
import platform
import usb
import os
from .port import Port

PID = 'USB VID:PID=0403:FAF0'  # Change to correct value
SER = ['28252040', '28252154', '28253011']  # Change to correct value


def find_stages(pid=PID, ser=SER): #pid=None, ser=None):
    #if (pid is not None) and (ser is not None):
    # Find stages matching provided USB PID and serial number
    com_ports_list = list(comports())
    motor_port = None
    for port in com_ports_list:
        for serial in ser:
            print(port[2], serial)
            if port[2].startswith(pid) and port[2].find(serial) != -1:
                print("found")
                motor_port = port[0]  # Motor found by VID/PID match.
                break  # stop searching-- we are done.
    p = Port.create(motor_port, serial)

    #else:
    #    # Use standard find_stages() if no USB PID nor serial number are provided
    #    serial_ports = [(x[0], x[1], dict(y.split('=', 1) for y in x[2].split(' ') if '=' in y)) for x in comports()]
    #    # The loop below will find all identified stage controllers and
    #    for dev in usb.core.find(find_all=True, custom_match= lambda x: x.bDeviceClass != 9):

    #        if platform.system() == 'Linux':
    #            # this is to check if the operating system is Linux, if not the code won't work. N.B. codename of MacOS
    #            # is Darwin.
    #            port_candidates = [x[0] for x in serial_ports if x[2].get('SER', None) == dev.serial_number]
    #            # expected value for port_candidates is:
    #            # ['/dev/ttyUSB0'], which is the serial port to communicate with the controller through

    #        else:
    #            raise NotImplementedError("Your operating system is not supported. " \
    #                "PyStage_APT only works on Linux.")

    #        try:
    #            #FIXME: this avoids an error related to https://github.com/walac/pyusb/issues/139
    #            #FIXME: this could maybe be solved in a better way?
    #            dev._langids = (1033, )
    #            # KDC101 3-port is recognized as FTDI in newer kernels
    #            if not (dev.manufacturer == 'Thorlabs' or dev.manufacturer == 'FTDI'):
    #                ### raise Exception('No manufacturer found')
    #                continue ### until the device is found
    #        except usb.core.USBError:
    #            print("No stage controller found: make sure your device is connected")
    #            break

    #        # we make sure at each iteration only one port entry is contained in port candidates
    #        # i.e. the ports are accessed one by one NOT all at one go.
    #        assert len(port_candidates) == 1

    #        port = port_candidates[0]
    #        p = Port.create(port, dev.serial_number)

    for stage in p.get_stages().values():
        # generator type
        yield stage


if __name__ == '__main__':
    print(list(find_stages()))

# Example device:
# Manufacturer                1. Thorlabs
# Product                     2. APT DC Motor Controller
# Serial No.                  3. 83844171
